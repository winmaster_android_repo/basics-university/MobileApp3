package katarzyna.mobileapp3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Lista1 extends AppCompatActivity implements AdapterView.OnItemClickListener
{
    String [] list= {"Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień" , "Wrzesień", "Październik", "Listopad", "Grudzień"};
    ListView lista1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista1);
        lista1=(ListView)findViewById(R.id.listViewListaProsta);
        lista1.setOnItemClickListener(this);
        ArrayAdapter<String> adapter2= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        lista1.setAdapter(adapter2);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        String val =(String) adapterView.getItemAtPosition(i);
        Toast.makeText(getApplicationContext(),val, Toast.LENGTH_SHORT).show();
    }
}
