package katarzyna.mobileapp3;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import static katarzyna.mobileapp3.MainActivity.*;

public class Lista3 extends AppCompatActivity
{
    ArrayList<Boolean> isChecked=new ArrayList<>();
    public String[] ltxt1 = {"Pozycja nr 1", "Pozycja nr 2", "Pozycja nr 3", "Pozycja nr 4","Pozycja nr 5","Pozycja nr 6","Pozycja nr 7","Pozycja nr 8","Pozycja nr 9","Pozycja nr 10" };
    public String[] ltxt2 = {"Tekst 1", "Tekst 2", "Tekst 3","Tekst 4","Tekst 5","Tekst 6","Tekst 7","Tekst 8","Tekst 9","Tekst 10"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista3);
        myAdapter2 adapter = new myAdapter2();
        ListView lista3 = (ListView) findViewById(R.id.listView3);
        lista3.setAdapter(adapter);

        for(int i=0; i<10; i++)
        {
            isChecked.add(false);
        }
    }

    public class myAdapter2 extends BaseAdapter
    {
        private LayoutInflater inflater = null;

        public int getCount()
        {
            return ltxt1.length;
        }

        public Object getItem(int position)
        {
            return position;
        }

        public long getItemId(int position)
        {
            return position;
        }

        public View getView(final int poss, View cView, ViewGroup parent)
        {
            View mV;

            inflater = (LayoutInflater)
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (cView == null)
            {
                cView = inflater.inflate(R.layout.list_row, null);
            }
            mV = cView;
            // kilka ustawień elem.
            CheckBox cb=(CheckBox)mV.findViewById(R.id.lrow_checkBox);
            TextView tv1 = (TextView) mV.findViewById(R.id.row_tv1);
            tv1.setText(ltxt1[poss]);
            TextView tv2 = (TextView) mV.findViewById(R.id.row_tv2);
            tv2.setText(ltxt2[poss]);

            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                {
                    isChecked.set(poss,b);
                }
            });

            cb.setChecked(isChecked.get(poss));
            return mV;
        }

    }
}

