package katarzyna.mobileapp3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    String[] lista = {"Pozycja 1", "Pozycja 2", "Pozycja 3"};
    String[] p = {"1", " 2", " 3"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner opcje = (Spinner) findViewById(R.id.spinner1);
        if (opcje != null)
        {
            opcje.setOnItemSelectedListener(this);
            ArrayAdapter<String> adapter = new
                    ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item,
                    lista);
            adapter.setDropDownViewResource(android.R.layout.
                    simple_spinner_dropdown_item);
            opcje.setAdapter(adapter);
        }

        final Intent intent1 = new Intent(this, Lista1.class);
        Button buttonListaProsta = (Button) findViewById(R.id.buttonListaProsta);
        buttonListaProsta.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                startActivity(intent1);
            }
        });

        final Intent intent2 = new Intent(this, Lista2.class);
        Button buttonListaMulti = (Button) findViewById(R.id.buttonListaMulti);
        buttonListaMulti.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                startActivity(intent2);
            }
        });

        final Intent intent3 = new Intent(this, Grid1.class);
        Button buttonGrid = (Button) findViewById(R.id.buttonGrid);
        buttonGrid.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                startActivity(intent3);
            }
        });

        final Intent intent4 = new Intent(this, Lista3.class);
        Button buttonListDost = (Button) findViewById(R.id.buttonListDost);
        buttonListDost.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                startActivity(intent4);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        Toast.makeText(getApplicationContext(),
                "Wybrałeś: "+p[i],
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}

