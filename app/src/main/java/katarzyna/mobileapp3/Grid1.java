package katarzyna.mobileapp3;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class Grid1 extends AppCompatActivity implements AdapterView.OnItemClickListener
{
    String [] list= {"Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień" , "Wrzesień", "Październik", "Listopad", "Grudzień"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid1);

        GridView gridView = (GridView) findViewById(R.id.gridView1);
        gridView.setAdapter(new myAdapter(this));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        String val =(String) adapterView.getItemAtPosition(i);
        Toast.makeText(getApplicationContext(),val, Toast.LENGTH_SHORT).show();
    }
}

